import React, { Component } from 'react'

export default class skill extends Component {
    render() {
        return (
            <div className="home">
                <div className="education-layout">
                    <h3><span>S</span>kills</h3>
                    <div className="about-table p-1 animate__animated animate__bounceInDown">
                        <div className="row">
                            <div className="col-lg-6">
                                <ul>
                                    <li className="animate__animated animate__bounceInLeft animate__delay-1s">Languages: C, C++, PHP, JavaScript, HTML, Basic Python </li>
                                    <li className="animate__animated animate__bounceInLeft animate__delay-2s">Framework: Codeigniter</li>
                                    <li className="animate__animated animate__bounceInLeft animate__delay-3s">CMS: Basic Wordpress </li>
                                    <li className="animate__animated animate__bounceInLeft animate__delay-4s">Others: API, JQuery, AJAX, VCS</li>
                                </ul>
                            </div>
                            <div className="col-lg-6">
                                <ul>
                                    <li className="animate__animated animate__bounceInRight animate__delay-1s">Stylesheet: CSS </li>
                                    <li className="animate__animated animate__bounceInRight animate__delay-2s">Database:  MySQL </li>
                                    <li className="animate__animated animate__bounceInRight animate__delay-3s">OSMS: Basic Opencart </li>
                                    <li className="animate__animated animate__bounceInRight animate__delay-4s">Interpersonal: Teamwork,  Troubleshooting, Patience </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
