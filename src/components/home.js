import React, { Component } from 'react';
import DP from '../profile.jpeg';

export default class Home extends Component {
    render() {
        return (
            <div className="home">
                <div className="home-left">
                    <h3>Hello, <br></br>I'm <span>Irfanul Kalam Chowdhury</span></h3>
                    <p>I am an adaptable and innovative qualified Computer Engineer with developing web based applications.</p>
                </div>
                <div className="divider animate__animated animate__zoomInDown">

                </div>
                <div className="home-right">
                    <img src={DP} alt="Profile"></img>
                    
                </div>
            </div>
        )
    }
}
