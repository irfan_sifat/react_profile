import React, { Component } from 'react';

export default class Portfolio extends Component {
    render() {
        return (
            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                {/* <ol className="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol> */}
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <div className="row container portfolio-page">
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>Auto&nbsp;Irrigation&nbsp;System</b></label>
                                <p>This project is on Automatic Irrigation System in Greenhouse Using Arduino & Moisture Sensor.</p>
                                {/* <br></br> */}
                                <p>Visit: <a href="https://www.youtube.com/watch?v=yYzVZfUBJQo" target="_blank" rel="noopener noreferrer"><i className="fa fa-youtube fa-2x"></i></a></p>
                            </div>
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>Workstation</b></label>
                                <p>It's a web based application to easy our daily life.</p>
                                <br></br>
                                <p>Visit: <a href="https://gitlab.com/irfan_sifat/workstation" target="_blank" rel="noopener noreferrer"><i className="fa fa-gitlab fa-2x"></i></a></p>
                            </div>
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>Ghuri</b></label>
                                <p>It's an online multi vendor site. Anyone can register as a seller or buyer both.</p>
                                {/* <br></br> */}
                                <p>Visit: <a href="https://gitlab.com/irfan_sifat/ghuri" target="_blank" rel="noopener noreferrer"><i className="fa fa-gitlab fa-2x"></i></a></p>
                            </div>
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>JSC&nbsp;System</b></label>
                                <p>Preview/Create dynamic invoice for JSC.</p>
                                {/* <br></br> */}
                                <p>Visit: <a href="https://gitlab.com/irfan_sifat/jsc_system" target="_blank" rel="noopener noreferrer"><i className="fa fa-gitlab fa-2x"></i></a></p>
                            </div>
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>ReactJS&nbsp;Calculator</b></label>
                                <p>Simple calculator and Human Weight calculator.</p>
                                {/* <br></br> */}
                                <p>Visit: <a href="https://gitlab.com/irfan_sifat/reactjs_calculator" target="_blank" rel="noopener noreferrer"><i className="fa fa-gitlab fa-2x"></i></a></p>
                            </div>
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>Profile&nbsp;Site</b></label>
                                <p>My first personal profile site with HTML, CSS & JS.</p>
                                {/* <br></br> */}
                                <p>Visit: <a href="https://gitlab.com/irfan_sifat/react_profile" target="_blank" rel="noopener noreferrer"><i className="fa fa-gitlab fa-2x"></i></a></p>
                            </div>
                        </div>

                    </div>
                    <div className="carousel-item">
                        <div className="row container portfolio-page">
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>DMAS</b></label>
                                <p>Document Management & Approval System. Create dynamic form, flow and others.</p>
                                {/* <br></br> */}
                                {/* <p>Visit: <a href="https://www.youtube.com/watch?v=yYzVZfUBJQo" target="_blank"><i className="fa fa-youtube fa-2x"></i></a></p> */}
                                <p className="text-warning"><b># Professional Project</b></p>
                            </div>
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>Easy&nbsp;Appointment</b></label>
                                <p>It's an CRM system for doctor's appointment.</p>
                                <br></br>
                                {/* <p>Visit: <a href="https://gitlab.com/irfan_sifat/workstation" target="_blank"><i className="fa fa-gitlab fa-2x"></i></a></p> */}
                                <p className="text-warning"><b># Professional Project</b></p>
                            </div>
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>Project&nbsp;Management</b></label>
                                <p>It's a project management system for track any project by inserting sprints.</p>
                                {/* <br></br> */}
                                {/* <p>Visit: <a href="https://gitlab.com/irfan_sifat/ghuri" target="_blank"><i className="fa fa-gitlab fa-2x"></i></a></p> */}
                                <p className="text-warning"><b># Professional Project</b></p>
                            </div>
                            {/* <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>JSC&nbsp;System</b></label>
                                <p>Preview/Create dynamic invoice for JSC.</p>
                                <br></br>
                                <p>Visit: <a href="https://gitlab.com/irfan_sifat/ghuri" target="_blank" rel="noopener noreferrer"><i className="fa fa-gitlab fa-2x"></i></a></p>
                            </div>
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>ReactJS&nbsp;Calculator</b></label>
                                <p>Simple calculator and Human Weight calculator.</p>
                                <br></br>
                                <p>Visit: <a href="https://gitlab.com/irfan_sifat/ghuri" target="_blank" rel="noopener noreferrer"><i className="fa fa-gitlab fa-2x"></i></a></p>
                            </div>
                            <div className="col-lg-3 col-sm-12 p-2">
                                <label className="portfolio-label"><b>Profile&nbsp;Site</b></label>
                                <p>My first personal profile site with HTML, CSS & JS.</p>
                                <br></br>
                                <p>Visit: <a href="https://gitlab.com/irfan_sifat/ghuri" target="_blank" rel="noopener noreferrer"><i className="fa fa-gitlab fa-2x"></i></a></p>
                            </div> */}
                        </div>

                    </div>
                    {/* <div className="carousel-item">
                        <img className="d-block w-100" src="..." alt="Third slide"></img>
                    </div> */}
                </div>
                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="sr-only">Previous</span>
                </a>
                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="sr-only">Next</span>
                </a>
            </div>
        )
    }
}
