import React, { Component } from 'react'

export default class experience extends Component {
    render() {
        return (
            <div className="home">
                <div className="education-layout">
                    <h3><span>E</span>xpertise</h3>
                    <div className="about-table p-1 animate__animated animate__bounceInDown">
                        <div className="each-edu">
                            <h6>December 2019 – Present </h6>
                            <h6> My responsibility is to design and develop Web Applications. I've used PHP Codeigniter Framework and WordPress.</h6>
                            <h5><a href="http://az-neo.com/" target="_blank" rel="noopener noreferrer">Azneo Limited</a></h5>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
