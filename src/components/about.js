import React, { Component } from 'react';

export default class About extends Component {
    render() {
        return (
            <div className="home">
                <div className="home-left">
                    <h3><span>A</span>bout <span>M</span>e</h3>
                    <table className="about-table">
                        <tbody><tr>
                            <td><b>Name</b></td>
                            <td>:</td>
                            <td>M. Irfanul Kalam Chowdhury</td>
                        </tr>

                            <tr>
                                <td><b>Birth</b></td>
                                <td>:</td>
                                <td>May 28, 1996</td>
                            </tr>

                            <tr>
                                <td><b>Address</b></td>
                                <td>:</td>
                                <td>Equity Central, 42/43 Momin Road, Kotwali, Chattogram </td>
                            </tr>

                            <tr>
                                <td><b>Email</b></td>
                                <td>:</td>
                                <td>msifat5@gmail.com</td>
                            </tr>

                            <tr>
                                <td><b>Phone</b></td>
                                <td>:&nbsp;</td>
                                <td>+8801718339135</td>
                            </tr>

                            <tr>
                                <td><b>Website</b>&nbsp;</td>
                                <td>:</td>
                                <td><a href="https://irfansifat.netlify.app/" target="_blank" rel="noopener noreferrer">https://irfansifat.netlify.app/</a></td>
                            </tr>

                            <tr>
                                <td><b>Gitlab</b>&nbsp;</td>
                                <td>:</td>
                                <td><a href="https://gitlab.com/irfan_sifat" target="_blank" rel="noopener noreferrer">@irfan_sifat </a></td>
                            </tr>
                        </tbody></table>
                </div>

                <div className="divider animate__animated animate__zoomInDown">
                </div>
                
                <div className="home-right">
                    <div className="about-table">
                        <h3>&nbsp;&nbsp;Personal Interest</h3>
                        <ul>
                            <li>Socializing with friends and family</li>
                            <li>Computing</li>
                            <li>Playing Games, Cricket, Football, Badminton&nbsp;</li>
                            <li>Driving & Travelling</li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
