import React, { Component } from 'react';

export default class About extends Component {
    render() {
        return (
            <div className="home">
                <div className="education-layout">
                    <h3><span>Q</span>ualifi<span>C</span>ations</h3>
                    <div className="about-table p-1 animate__animated animate__bounceInDown">
                        <div className="each-edu">
                            <h6>SSC/O Level</h6>
                            <h5>Shahwali Ullah Institute <label className="text-warning">[2012]</label></h5>
                            {/* <h6>2012</h6> */}
                            {/* <hr></hr> */}
                        </div>
                        <br></br>
                        <div className="each-edu">
                            <h6>HSC/A Level</h6>
                            <h5>B.A.F Shaheen Collage Chittagong <label className="text-warning">[2014]</label></h5>
                            {/* <h6>2014</h6> */}
                            {/* <hr></hr> */}
                        </div>
                        <br></br>
                        <div className="each-edu">
                            <h6>Bsc in Computer Science & Engineering</h6>
                            <h5>International Islamic University Chittagong <label className="text-warning">[2019]</label></h5>
                            {/* <h6>2019</h6> */}
                        </div>
                    </div>
                </div>

                {/* <div className="divider animate__animated animate__zoomInDown">
                </div> */}

                {/* <div className="home-right">
                    <div className="about-table">
                        <h3>&nbsp;&nbsp;Personal Interest</h3>
                        <ul>
                            <li>Socializing with friends and family</li>
                            <li>Computing</li>
                            <li>Playing Games, Cricket, Football, Badminton&nbsp;</li>
                            <li>Driving & Travelling</li>
                        </ul>
                    </div>
                </div> */}
            </div>
        )
    }
}
