import React from 'react';
import logo from './bg-img.png';
import './App.css';
import { BrowserRouter as Router, Switch, Route, NavLink } from "react-router-dom";

import Home from './components/home';
import About from './components/about';
import Education from './components/education';
import Portfolio from './components/portfolio';
import Skill from './components/skill';
import Experience from './components/experience';

function App() {
  return (
    <Router>
      <div className="App">
        <div className="top-mail">
          <span><a href="mailto:msifat5@gmail.com?subject = Feedback"><i className="fa fa-envelope animate__animated animate__bounce"></i></a></span>
          {/* <span><a href="mailto:msifat5@gmail.com?subject = Feedback"><i type="button" data-toggle="modal" data-target="#exampleModal" className="fa fa-envelope animate__animated animate__bounce"></i></a></span> */}
          {/* <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <form>
                    <div className="form-group">
                      <label for="con-name">Name</label>
                      <input type="text" id="con-name" name="con-name" className="form-control" required></input>
                    </div>
                    <div className="form-group">
                      <label for="con-email">Email</label>
                      <input type="email" id="con-email" name="con-email" className="form-control" required></input>
                    </div>
                  </form>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" className="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div> */}
        </div>
        <div className="left-sidebar">
          <a href="https://www.facebook.com/ikalam.chy" target="_blank" rel="noopener noreferrer"><span><i className="animate__animated animate__rollIn fa fa-facebook-square"></i></span></a><br></br>
          <a href="https://www.linkedin.com/in/irfan-sifat" target="_blank" rel="noopener noreferrer"><span><i className="animate__animated animate__rollIn fa fa-linkedin"></i></span></a><br></br>
          <a href="https://twitter.com/msifat1" target="_blank" rel="noopener noreferrer"><span><i className="animate__animated animate__rollIn fa fa-twitter"></i></span></a>
        </div>
        <div className="image-container">
          <img src={logo} alt="BG_Image"></img>
          <div className="after">
            <div>
              <Switch>
                <Route path="/about">
                  <About />
                </Route>
                <Route path="/education">
                  <Education />
                </Route>
                <Route path="/skill">
                  <Skill />
                </Route>
                <Route path="/experience">
                  <Experience />
                </Route>
                <Route path="/portfolio">
                  <Portfolio />
                </Route>
                <Route path="/">
                  <Home />
                </Route>
              </Switch>
            </div>
          </div>
        </div>

        <div className="bottom-menu">
          <div className="cv-section">
            <span className="cv-section-text"><i className="animate__animated animate__bounce">CV</i></span>
            <span className="cv-section-icon"><a href="M. Irfanul Kalam Chowdhury.pdf" download=""><i className="fa fa-download animate__animated animate__rubberBand"></i></a></span>
          </div>
          <div className="menu-section animate__animated animate__slideInRight">
            <NavLink exact activeStyle={{ color: 'yellow' }} to="/"> <h3>__Home</h3></NavLink>
            <NavLink activeStyle={{ color: 'yellow' }} to="/skill"> <h3>__Skills</h3></NavLink>
            <NavLink activeStyle={{ color: 'yellow' }} to="/portfolio"> <h3>__Portfolio</h3></NavLink>
            <NavLink activeStyle={{ color: 'yellow' }} to="/about"><h3>__About</h3></NavLink>
            <NavLink activeStyle={{ color: 'yellow' }} to="/education"> <h3>__Education</h3></NavLink>
            <NavLink activeStyle={{ color: 'yellow' }} to="/experience"><h3>__Experience</h3></NavLink>
          </div>
          <br></br>
          <div className="menu-section_mbl">
            <div id="circularMenu" className="circular-menu">

              <p className="floating-btn" onClick={toggleClass}>
                <i className="fa fa-plus"></i>
              </p>

              <menu className="items-wrapper">
                <NavLink exact to="/" className="menu-item"><p >H</p></NavLink>
                <NavLink to="/skill" className="menu-item"><p >S</p></NavLink>
                <NavLink to="/portfolio" className="menu-item"><p >P</p></NavLink>
                <NavLink to="/about" className="menu-item"><p >A</p></NavLink>
                <NavLink to="/education" className="menu-item"><p >E</p></NavLink>
                <NavLink to="/experience" className="menu-item"><p >Ex</p></NavLink>
              </menu>

            </div>
          </div>
        </div>
      </div>
    </Router>
  );
}

function toggleClass() {
  document.getElementById('circularMenu').classList.toggle('active');
}


export default App;
